//
//  main.m
//  LOLCat Viewer
//
//  Created by Wayne Ohmer on 4/10/14.
//  Copyright (c) 2014 Wayne Ohmer. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
