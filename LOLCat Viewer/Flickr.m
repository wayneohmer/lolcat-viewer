//
//  Flickr.m
//  Flickr Search
//
//  Created by Brandon Trebitowski on 6/28/12.
//  Copyright (c) 2012 Brandon Trebitowski. All rights reserved.
//

#import "Flickr.h"
#import "FlickrPhoto.h"
#define kFlickrAPIKey @"4961c4c171778f54c2d5ff21cb9baecc"
// secret 4961c4c171778f54c2d5ff21cb9baecc

@interface Flickr ()

@end

@implementation Flickr
//Wayne - added withNumberOfResults parmeter. It was hard coded to 20
+ (NSString *)flickrSearchURLForSearchTerm:(NSString *) searchTerm withNumberOfResults:(NSInteger)numberOfResults
{
    searchTerm = [searchTerm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&text=%@&per_page=%@&format=json&nojsoncallback=1",kFlickrAPIKey,searchTerm,@(numberOfResults)];
}

+ (NSString *)flickrPhotoURLForFlickrPhoto:(FlickrPhoto *) flickrPhoto withSize:(NSString *) size
{
    if(!size)
    {
        size = @"m";
    }
    //Wayne - changed %d's to "boxed" strings. Makes it 32/64 bit compatible.
    return [NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%lld_%@_%@.jpg",@(flickrPhoto.farm),@(flickrPhoto.server),flickrPhoto.photoID,flickrPhoto.secret,size];
}

//Wayne - added withNumberOfReults and saveingThumbnails parameters
- (void)searchFlickrForTerm:(NSString *) searchTerm withNumberOfReults:(NSInteger)numberOfResults saveingThumbnails:(BOOL)savingThumbnails completionBlock:(FlickrSearchCompletionBlock) completionBlock
{
    NSString *searchURL = [Flickr flickrSearchURLForSearchTerm:searchTerm withNumberOfResults:numberOfResults];
    //Wayne - implemented NSURLConnection. This puts the task on the mainQueue. old code here:
    //    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //    dispatch_async(queue, ^{
    //        NSError *error = nil;
    //        NSString *searchResultString = [NSString stringWithContentsOfURL:[NSURL URLWithString:searchURL]
    //                                                           encoding:NSUTF8StringEncoding
    //                                                              error:&error];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:searchURL]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response,NSData *data,NSError *error) {
        
        NSString *searchResultString = [[NSString alloc ]initWithData:data encoding:NSUTF8StringEncoding];
        if (error != nil) {
            completionBlock(searchTerm,nil,error);
        }
        else
        {
            // Parse the JSON Response
            NSData *jsonData = [searchResultString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *searchResultsDict = [NSJSONSerialization JSONObjectWithData:jsonData  options:kNilOptions  error:&error];
            if(error != nil)
            {
                completionBlock(searchTerm,nil,error);
            }
            else
            {
                NSString * status = searchResultsDict[@"stat"];
                if ([status isEqualToString:@"fail"]) {
                    NSError *error = [[NSError alloc] initWithDomain:@"FlickrSearch" code:0 userInfo:@{NSLocalizedFailureReasonErrorKey: searchResultsDict[@"message"]}];
                    completionBlock(searchTerm, nil, error);
                } else {
                    
                    NSArray *objPhotos = searchResultsDict[@"photos"][@"photo"];
                    NSMutableArray *flickrPhotos = [@[] mutableCopy];
                    for(NSMutableDictionary *objPhoto in objPhotos)
                    {
                        FlickrPhoto *photo = [[FlickrPhoto alloc] init];
                        photo.farm = [objPhoto[@"farm"] intValue];
                        photo.server = [objPhoto[@"server"] intValue];
                        photo.secret = objPhoto[@"secret"];
                        photo.photoID = [objPhoto[@"id"] longLongValue];
                        //Wayne - added savingThumbnails bool to save time when we arnen't using thumbnails.
                        if (savingThumbnails)
                        {
                            NSString *searchURL = [Flickr flickrPhotoURLForFlickrPhoto:photo withSize:@"m"];
                            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:searchURL] options:0 error:&error];
                            UIImage *image = [UIImage imageWithData:imageData];
                            photo.thumbnail = image;
                        }
                        [flickrPhotos addObject:photo];
                    }
                    completionBlock(searchTerm,flickrPhotos,nil);
                }
            }
        }
    } ];
}
//Wayne - I do not use this method. Created my own in the View controller so I could use it as a delegate. Mostly for updateing status bar.
//+ (void)loadImageForPhoto:(FlickrPhoto *)flickrPhoto thumbnail:(BOOL)thumbnail completionBlock:(FlickrPhotoCompletionBlock) completionBlock
//{
//    
//    NSString *size = thumbnail ? @"m" : @"b";
//    
//    NSString *searchURL = [Flickr flickrPhotoURLForFlickrPhoto:flickrPhoto withSize:size];
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    
//    dispatch_async(queue, ^{
//        NSError *error = nil;
//        
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:searchURL]
//                                                  options:0
//                                                    error:&error];
//        if(error)
//        {
//            completionBlock(nil,error);
//        }
//        else
//        {
//            UIImage *image = [UIImage imageWithData:imageData];
//            if([size isEqualToString:@"m"])
//            {
//                flickrPhoto.thumbnail = image;
//            }
//            else
//            {
//                flickrPhoto.largeImage = image;
//            }
//            completionBlock(image,nil);
//        }
//        
//    });
//}

@end
