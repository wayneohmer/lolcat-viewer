//
//  CatPicutreViewController.m
//  LOLCat Viewer
//
//  Created by Wayne Ohmer on 4/10/14.
//  Copyright (c) 2014 Wayne Ohmer. All rights reserved.
//

#import "catPicutreViewController.h"
#define Max_Saved_Pictures 100
#define Animation_Duration 0.3

@interface catPicutreViewController () <NSURLConnectionDelegate,NSURLConnectionDataDelegate>

@property (nonatomic,strong) UIImageView *catImageView;
@property (nonatomic,strong) UITapGestureRecognizer *catImageViewTapRecognizer;
@property (nonatomic,strong) UILabel *instructionLabel;
@property (nonatomic,strong) UILabel *progressLabel;
@property (nonatomic,strong) UIActivityIndicatorView *spinnerView;
@property (nonatomic,strong) UIProgressView *catUploadProgressView;
@property (nonatomic,strong) UIButton *backButton;
@property (nonatomic,strong) UIButton *forwardButton;
@property (nonatomic,strong) UIProgressView *progressBar;
@property (nonatomic,strong) UIImage *loadingImage;
@property (nonatomic,strong) NSURLConnection *loadingImageConnecion;
@property (nonatomic,strong) NSMutableData *loadingImageData;
@property (nonatomic,assign) long long loadingImageExpectedBytes;
@property (nonatomic,strong) NSMutableArray *catPictureArray;
@property (nonatomic,assign) NSUInteger currentPictureIndex;
@property (nonatomic,strong) NSMutableArray *portraitConstraints;
@property (nonatomic,strong) NSMutableArray *landscapeConstraints;
@property (nonatomic,strong) NSLayoutConstraint *catImageXConstraint;
@property (nonatomic,strong) NSLayoutConstraint *catImageoffScreenLeft;
@property (nonatomic,strong) NSLayoutConstraint *catImageoffScreenRight;
@property (nonatomic,strong) NSLayoutConstraint *catImageCenter;
@property (nonatomic, strong) Flickr *flickr;

@end

@implementation catPicutreViewController

NSString* const portraitInstructions = @"Touch Picture For New LOLCat";
NSString* const mySearchTerm = @"LOLcat";
NSString* const searchingString = @"Searching";
NSString* const loadingString = @"Loading";
NSString* const searchFailedString = @"Search Failed";
NSString* const failCatImage = @"failCat.jpg";
NSString* const pictureFrameImage = @"wilma.png";
NSString* const previousArrowImage = @"previousArrow";
NSString* const nextArrowImage = @"nextArrow";
NSString* const onePictureInstructions = @"Touch Picture Again To Browse";

#pragma mark - Set Up Interface
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.flickr = [[Flickr alloc] init];
    self.catPictureArray = [[NSMutableArray alloc] init];
    self.portraitConstraints = [[NSMutableArray alloc] init];
    self.landscapeConstraints = [[NSMutableArray alloc] init];
    self.catPictureArray = [[NSMutableArray alloc] init];
    
    self.catImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:pictureFrameImage]];
    self.catImageView.layer.borderColor = [UIColor blackColor].CGColor;
    self.catImageView.layer.borderWidth = 2.0;
    self.catImageView.contentMode = UIViewContentModeCenter;
    self.catImageView.contentMode = UIViewContentModeScaleAspectFit;
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(catPictureSwipe:)];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(catPictureSwipe:)];
    [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    UISwipeGestureRecognizer *upSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(catPictureSwipe:)];
    [upSwipe setDirection:UISwipeGestureRecognizerDirectionUp];
    UISwipeGestureRecognizer *downSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(catPictureSwipe:)];
    [downSwipe setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.catImageView addGestureRecognizer:leftSwipe];
    [self.catImageView addGestureRecognizer:rightSwipe];
    [self.catImageView addGestureRecognizer:upSwipe];
    [self.catImageView addGestureRecognizer:downSwipe];
    self.catImageViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(catPictureTouch)];
    [self.catImageView addGestureRecognizer:self.catImageViewTapRecognizer];
    [self.catImageView setMultipleTouchEnabled:YES];
    [self.catImageView setUserInteractionEnabled:YES];
    [self.catImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:self.catImageView];
    
    self.instructionLabel = [[UILabel alloc] init];
    self.instructionLabel.text = NSLocalizedString(portraitInstructions, nil);
    self.instructionLabel.textAlignment = NSTextAlignmentCenter;
    [self.instructionLabel setFont:[UIFont systemFontOfSize:12]];
    [self.instructionLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:self.instructionLabel];
    
    self.progressLabel = [[UILabel alloc] init];
    self.progressLabel.text = NSLocalizedString(@"", nil);
    self.progressLabel.textAlignment = NSTextAlignmentCenter;
    [self.progressLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:self.progressLabel];
    
    self.spinnerView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.spinnerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:self.spinnerView];
    
    self.backButton = [[UIButton alloc] init];
    [self.backButton setImage:[UIImage imageNamed:previousArrowImage] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(previousPicture) forControlEvents:UIControlEventTouchUpInside];
    self.backButton.enabled = NO;
    [self.backButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:self.backButton];
    
    self.forwardButton = [[UIButton alloc] init];
    [self.forwardButton setImage:[UIImage imageNamed:nextArrowImage] forState:UIControlStateNormal];
    [self.forwardButton addTarget:self action:@selector(nextPicture) forControlEvents:UIControlEventTouchUpInside];
    self.forwardButton.enabled = NO;
    [self.forwardButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:self.forwardButton];
    
    self.progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    [self.progressBar setProgress:0.0];
    self.progressBar.hidden = YES;
    [self.progressBar setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:self.progressBar];
    
    [self setupAllContstraints];
}

-(void) setupAllContstraints
{
//Add contraints directly for those that are the same in Portrait and Landscape.
//Add to array if different in portrait/landscape.
    //Ceter catImageView and make in 85% height of self.view and 80-95% width, depending on orientation.
    //X contraints is saved as property to support animation.
    self.catImageXConstraint = [NSLayoutConstraint constraintWithItem:self.catImageView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0];
    
    
    [self.view addConstraint:self.catImageXConstraint];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.catImageView
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.catImageView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:0.85
                                                           constant:0]];
    
    [self.portraitConstraints addObject:[NSLayoutConstraint constraintWithItem:self.catImageView
                                                                     attribute:NSLayoutAttributeWidth
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.view
                                                                     attribute:NSLayoutAttributeWidth
                                                                    multiplier:0.95
                                                                      constant:0]];
    
    [self.landscapeConstraints addObject:[NSLayoutConstraint constraintWithItem:self.catImageView
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.view
                                                                      attribute:NSLayoutAttributeWidth
                                                                     multiplier:0.8
                                                                       constant:0]];
    //instructionsLabel - center x on catImageView, allign with Top of catImageView
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.instructionLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.instructionLabel
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0]];
    //progressLabel - center x on catImageView, 3 pixels below catImageView
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.progressLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.progressLabel
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:3]];
    //spinner - center on catImageView
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.spinnerView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.spinnerView
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0]];
    //progress bar - bottom of catImageView,align left with catImageView, width of catImageView
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.progressBar
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.progressBar
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.0
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.progressBar
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.catImageView
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0
                                                           constant:0]];
    
    //buttons Portrait, top = bottom of catImageView backButton left=left forwardButton right=right
    [self.portraitConstraints addObject:[NSLayoutConstraint constraintWithItem:self.backButton
                                                                     attribute:NSLayoutAttributeBottom
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.view
                                                                     attribute:NSLayoutAttributeBottom
                                                                    multiplier:1.0
                                                                      constant:-4]];
    
    [self.portraitConstraints addObject:[NSLayoutConstraint constraintWithItem:self.backButton
                                                                     attribute:NSLayoutAttributeLeft
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.view
                                                                     attribute:NSLayoutAttributeLeft
                                                                    multiplier:1.0
                                                                      constant:0]];
    
    [self.portraitConstraints addObject:[NSLayoutConstraint constraintWithItem:self.forwardButton
                                                                     attribute:NSLayoutAttributeBottom
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.view
                                                                     attribute:NSLayoutAttributeBottom
                                                                    multiplier:1.0
                                                                      constant:-4]];
    
    [self.portraitConstraints addObject:[NSLayoutConstraint constraintWithItem:self.forwardButton
                                                                     attribute:NSLayoutAttributeRight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.view
                                                                     attribute:NSLayoutAttributeRight
                                                                    multiplier:1.0
                                                                      constant:0]];
    
    //buttons landscape- center on catImageView y, 3 pixes to the left or right edge
    [self.landscapeConstraints addObject:[NSLayoutConstraint constraintWithItem:self.backButton
                                                                      attribute:NSLayoutAttributeCenterY
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.catImageView
                                                                      attribute:NSLayoutAttributeCenterY
                                                                     multiplier:1.0
                                                                       constant:0]];
    
    [self.landscapeConstraints addObject:[NSLayoutConstraint constraintWithItem:self.backButton
                                                                      attribute:NSLayoutAttributeRight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.catImageView
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1.0
                                                                       constant:-3]];
    
    [self.landscapeConstraints addObject:[NSLayoutConstraint constraintWithItem:self.forwardButton
                                                                      attribute:NSLayoutAttributeCenterY
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.catImageView
                                                                      attribute:NSLayoutAttributeCenterY
                                                                     multiplier:1.0
                                                                       constant:0]];
    
    [self.landscapeConstraints addObject:[NSLayoutConstraint constraintWithItem:self.forwardButton
                                                                      attribute:NSLayoutAttributeLeft
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.catImageView
                                                                      attribute:NSLayoutAttributeRight
                                                                     multiplier:1.0
                                                                       constant:3] ];
    //contriants for animation
    self.catImageoffScreenLeft = [NSLayoutConstraint constraintWithItem:self.catImageView
                                                              attribute:NSLayoutAttributeRight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view
                                                              attribute:NSLayoutAttributeLeft
                                                             multiplier:1.0
                                                               constant:0];
    self.catImageoffScreenRight = [NSLayoutConstraint constraintWithItem:self.catImageView
                                                               attribute:NSLayoutAttributeLeft
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.view
                                                               attribute:NSLayoutAttributeRight
                                                              multiplier:1.0
                                                                constant:0];
    self.catImageCenter = [NSLayoutConstraint constraintWithItem:self.catImageView
                                                       attribute:NSLayoutAttributeCenterX
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:self.view
                                                       attribute:NSLayoutAttributeCenterX
                                                      multiplier:1.0
                                                        constant:0];

    //start in portrait

    for (NSLayoutConstraint *constraint in self.portraitConstraints) {
        [self.view addConstraint:constraint];
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    switch (toInterfaceOrientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            [self.view removeConstraints:self.landscapeConstraints];
            [self.view addConstraints:self.portraitConstraints];
            [self.instructionLabel setFont:[UIFont systemFontOfSize:12]];
            break;
            
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            [self.instructionLabel setFont:[UIFont systemFontOfSize:8]];
            [self.view removeConstraints:self.portraitConstraints];
            [self.view addConstraints:self.landscapeConstraints];
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

#pragma mark - Handle Gestures.
-(void) previousPicture
{
    [self animateTransition:UISwipeGestureRecognizerDirectionRight];
}

-(void) nextPicture
{
    [self animateTransition:UISwipeGestureRecognizerDirectionLeft];
}

-(void) catPictureSwipe:(UISwipeGestureRecognizer *)swipe
{
    //I like using switch on enums. The compiler will warn you if you miss something.
    switch (swipe.direction) {
        case UISwipeGestureRecognizerDirectionLeft:
            if (self.forwardButton.enabled) {
                [self nextPicture];
            }
            break;
        case UISwipeGestureRecognizerDirectionRight:
            if (self.backButton.enabled) {
                [self previousPicture];
            }
            break;
        case UISwipeGestureRecognizerDirectionUp:
            self.currentPictureIndex = [self.catPictureArray count]-1;
            [self.catImageView setImage:[self.catPictureArray objectAtIndex:self.currentPictureIndex]];
            [self fixButtonStates];
            break;
        case UISwipeGestureRecognizerDirectionDown:
            self.currentPictureIndex = 0;
            [self.catImageView setImage:[self.catPictureArray objectAtIndex:self.currentPictureIndex]];
            [self fixButtonStates];
            break;
     }
}

- (void)catPictureTouch
{
    //Grab metadata from 100 lolcat photoes
    [self.flickr searchFlickrForTerm:mySearchTerm withNumberOfReults:100 saveingThumbnails:NO completionBlock:^(NSString *searchTerm, NSArray *results, NSError *error) {
        if(results && [results count] > 0 && error == nil)
        {
            // generate random number for 0 to number of actual results.
            FlickrPhoto *randomResult = [results objectAtIndex:arc4random()%[results count]];
            [self loadImageForPhoto:randomResult];
        }
        else
        {
            [self cleanupAfterDownloadWithSuccess:NO];
        }
     }];
    [self.spinnerView startAnimating];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    self.progressLabel.text = NSLocalizedString(searchingString, nil);
    //Disable click while new image is loading.
    [self.catImageView removeGestureRecognizer:self.catImageViewTapRecognizer];
}

#pragma mark - animation
- (void) animateTransition:(UISwipeGestureRecognizerDirection)direction
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:Animation_Duration
                     animations:^{
                         [self.view removeConstraint:self.catImageXConstraint];
                         if (direction == UISwipeGestureRecognizerDirectionLeft)
                         {
                             self.catImageXConstraint = self.catImageoffScreenLeft;
                         }
                         else
                         {
                             self.catImageXConstraint = self.catImageoffScreenRight;
                         }
                         [self.view addConstraint:self.catImageXConstraint];
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         // move image view to the other side
                         [self.view removeConstraint:self.catImageXConstraint];
                         if (direction == UISwipeGestureRecognizerDirectionLeft)
                         {
                             //change image
                             [self moveCatImageForward];
                             self.catImageXConstraint = self.catImageoffScreenRight;
                         }
                         else
                         {
                             //change image
                             [self moveCatImageBack];
                             self.catImageXConstraint = self.catImageoffScreenLeft;
                         }
                         [self.view addConstraint:self.catImageXConstraint];
                         [self animateTransitionBackIn];
                     }];
}

- (void) animateTransitionBackIn
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:Animation_Duration
                     animations:^{
                         [self.view removeConstraint:self.catImageXConstraint];
                         self.catImageXConstraint = self.catImageCenter;
                         [self.view addConstraint:self.catImageXConstraint];
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - Utility Methods
-(void) moveCatImageBack
{
    if (self.currentPictureIndex > 0) {
        self.currentPictureIndex--;
        [self.catImageView setImage:[self.catPictureArray objectAtIndex:self.currentPictureIndex]];
        [self fixButtonStates];
    }
}

-(void) moveCatImageForward
{
    if (self.currentPictureIndex < [self.catPictureArray count]-1 ) {
        self.currentPictureIndex++;
        [self.catImageView setImage:[self.catPictureArray objectAtIndex:self.currentPictureIndex]];
        [self fixButtonStates];
    }

}

-(void) fixButtonStates
{
//a bit inefficient, but it saves haveing situational code scattered throughout

    if ([self.catPictureArray count] > 1 && self.currentPictureIndex > 0) {
        self.backButton.enabled = YES;
    }else{
        self.backButton.enabled = NO;
    }
    if ([self.catPictureArray count] > self.currentPictureIndex+1) {
        self.forwardButton.enabled = YES;
    }else{
        self.forwardButton.enabled = NO;
    }
    if (self.forwardButton.enabled == NO && self.backButton.enabled == NO){
        self.progressLabel.text = NSLocalizedString(onePictureInstructions, nil);

    }
}

-(void) cleanupAfterDownloadWithSuccess:(BOOL) success
{
    if (success) {
        self.progressLabel.text = @"";
    }else{
        [self.catImageView setImage:[UIImage imageNamed:failCatImage]];
        self.progressLabel.text = NSLocalizedString(searchFailedString, nil);
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.spinnerView stopAnimating];
    [self fixButtonStates];
    [self.catImageView addGestureRecognizer:self.catImageViewTapRecognizer];
    self.progressBar.hidden = YES;
    [self.progressBar setProgress:0];
    self.loadingImageExpectedBytes = 0;
}

- (void)loadImageForPhoto:(FlickrPhoto *)flickrPhoto
{
    NSString *searchURL = [Flickr flickrPhotoURLForFlickrPhoto:flickrPhoto withSize:@"b"];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:searchURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    self.loadingImage = [[UIImage alloc] init];
    self.loadingImageData = [[NSMutableData alloc] initWithLength:0];
    self.loadingImageConnecion = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self startImmediately:YES];
    self.progressLabel.text = NSLocalizedString(loadingString, nil);
    self.progressBar.hidden = NO;

 }
#pragma mark - NSURLConnection Delegate Methods
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.loadingImageData setLength:0];
    self.loadingImageExpectedBytes = [response expectedContentLength];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.loadingImageData appendData:data];
    [self.progressBar setProgress:((float)[self.loadingImageData length] / (float)self.loadingImageExpectedBytes)];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self cleanupAfterDownloadWithSuccess:NO];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    UIImage *image = [UIImage imageWithData:self.loadingImageData];
    self.loadingImage= image;
    [self.catImageView setImage:self.loadingImage];
    [self.catPictureArray addObject:self.loadingImage];
    //limit number of pics saved.
    if ([self.catPictureArray count] > Max_Saved_Pictures) {
        [self.catPictureArray removeObjectAtIndex:0];
    }
    self.currentPictureIndex = [self.catPictureArray count]-1;
    [self cleanupAfterDownloadWithSuccess:YES];
 }
#pragma mark - Memory Warning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //Get rid of all cached pitures. With more time I would be smarter about it. Perhaps removeing half.  
    [self.catPictureArray removeAllObjects];
    self.currentPictureIndex = 0;
    [self fixButtonStates];
}

@end
