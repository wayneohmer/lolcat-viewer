# README #
This was the Challenge:

* Given that I am a user that has an iPhone.
* When I touch the screen in the area that looks like a rectangle or picture frame.
* Then the rectangle or picture frame loads with an image of a random LoLcat loaded from Flickr.
* Button / touch area on screen can be a simple rectangle (minimum 200x400)
* Asynchronous loading of the image over the network
* Provides user with loading status indicator
* Supports rotation
* Loads LoLcats from Flickr
* Please do not use Interface Builder ­ please create your custom views in code, not using interface builder.
* Bonus points for iPad compatibility

Here was my basic approach:

Draw an imageView with borders and add initial image of my wife's cat.

When imageView is tapped, get metadata for 100 Flickr Photos, randomly choose one, download the image, and put it into the imageView.

Then I went a little crazy and started saving previous images so you could browse through them with buttons.

Then I went more crazy and added swipe gestures. Left and right to browse, up takes you to the last image and down takes you to the first.

For more fun I animated the images changes

Considerations:

I found some basic Flickr API code here:

http://cdn5.raywenderlich.com/wp-content/uploads/2012/09/Flickr-Interface.zip

I put it in the Flickr Classes group to make it clear.  I have modified it a bit to be more useful to me.  

For the networking, I used asynchronous NSURLConnection in two different ways. For searching, I used a completion block since there is no progress information available in the search. For downloading the image, I used a delegate so I could implement a progress bar.

I make extensive use of NSLayoutConstraint to arrange the interface. The code is big and wordy but It puts everything where I want it. I have tested it in all of the iPhone and iPad simulators back to iOS 6.1 and it is functional. It looks best on a 4 inch iPhone running iOS 7.

The border on the imageView looks a bit odd since it rarely matches the size of the image. I thought this was better than distorting the image.

It handles failures in a humorous way.
